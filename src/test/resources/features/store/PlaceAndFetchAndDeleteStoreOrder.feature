Feature: The store order service places an order for pet


  @storeOrder
  Scenario Outline: Place a pet order

    When I place a pet order with details "<id>","<petId>","<quantity>","<shipDate>","<status>","<complete>"
    Then the order with details "<id>","<petId>","<quantity>","<shipDate>","<status>","<complete>" must be successfully placed

    Examples:
      | id | petId | quantity | shipDate   | status   | complete |
      | 1  | 1     | 1        | 2021-06-13 | approved | true     |
      | 1  | 1     | 1        | 2021-06-13 | approved | true     |


  @fetchOrder
  Scenario Outline: Find purchase order by ID

    Given a valid orderId "<orderId>"
    When I retrieve the details for order "<orderId>"
    Then I should see the details for order "<orderId>"

    Examples:

      | orderId |  |
      | 1       |  |

  @fetchOrder-Invalid
  Scenario Outline: Find purchase order by ID

    Given an invalid orderId "<orderId>"
    When I retrieve the details for order "<orderId>"
    Then I should see an error "<errorMessage>",errorCode "<errorCode>" in the response for order "<orderId>"

    Examples:

      | orderId |  | errorMessage    | errorCode |
      | 6       |  | order not found | 404       |


  @deleteOrder
  Scenario Outline: Delete purchase order by ID

    Given a valid orderId "<orderId>"
    When I delete the details for order "<orderId>"
    Then order "<orderId>" should be deleted

    Examples:

      | orderId |  |
      | 1       |  |
      | -2005   |  |


  @deleteOrder-invalid-order-Id
  Scenario Outline: Delete purchase order by ID

    When I delete the details for order "<orderId>"
    Then I should see an error "<errorMessage>",errorCode "<errorCode>" in the response for order "<orderId>"

    Examples:

      | orderId |  | errorMessage | errorCode |
      | @       |  | Input error  | 400       |




