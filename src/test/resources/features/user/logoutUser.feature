Feature: The user/logout service should logout the user from the petstore system


  @logout
  Scenario Outline: logout user from the petstore system

    Given a userName "<userName>" is logged in
    When userName "<userName>" logs out
    Then userName "<userName>" should be logged out


    Examples:

      | userName | password |
      | pavan    | pavan    |

