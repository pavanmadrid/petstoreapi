Feature: The user service creates an user


  @createUserArray
  Scenario Outline: Create new users

    When I create multiple users of userNames "<userNames>"
    Then multiple users of userNames "<userNames>" should be created

    Examples:

      | userNames  |
      | pavan;vasu |



