Feature: The user service creates an user


  @createUser
  Scenario Outline: Create a new user

    When I create a new user of userName "<userName>" ,id "<id>",firstName "<firstName>",lastName "<lastName>",email "<email>",password "<password>",phone "<phone>",userStatus "<userStatus>"
    Then a new user of userName "<userName>" is successfully created

    Examples:

      | userName | id  | firstName | lastName | email          | password | phone      | userStatus |
      | pavan    | 101 | pavan     | pavan    | pavan@test.com | password | 9782129988 | 1          |


  @getUser-valid
  Scenario Outline: get user by username

    Given a valid username "<userName>"
    When I fetch the details for user "<userName>"
    Then I should see the details of user "<userName>"

    Examples:

      | userName |  |
      | pavan    |  |

  @getUser-invalid
  Scenario Outline: get user by username

    When I fetch the details for an invalid user "<userName>"
    Then I should see an error in the response for user "<userName>"

    Examples:

      | userName    |  |
      | invalidUser |  |


  @updateUser-valid
  Scenario Outline: get user by username

    Given a valid username "<userName>"
    When I update the details for user "<userName>"
    Then I should see the updated details of user "<userName>"

    Examples:

      | userName |  |
      | pavan    |  |


  @delete-user-valid
  Scenario Outline: delete user by username

    Given a valid username "<userName>"
    When I delete the user "<userName>"
    Then user "<userName>" should be deleted

    Examples:

      | userName |  |
      | pavan    |  |