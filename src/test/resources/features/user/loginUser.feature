Feature: The user/login service should log the user into the petstore system


  @login
  Scenario Outline: log user into the petstore system

    Given a valid username "<userName>"
    And a valid password "<password>"
    When I log in to the petstore application with user "<userName>" and password "<password>"
    Then login should be successful

    Examples:

      | userName | password |
      | pavan    | pavan    |


