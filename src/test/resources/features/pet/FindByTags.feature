Feature: The pet findByTags service returns pets by tag name


  @getPetByTag
  Scenario Outline: Get pet by tag names

    When I retrieve pet by tagName "<tagName>"
    Then I should see details of pet of tagName "<tagName>"

    Examples:

      | tagName |
      | dogs    |
      | cats    |


  @getPetByTag-invalid
  Scenario Outline: Get pet by tagName-invalid

    When I retrieve pet by an invalid tagName "<tagName>"
    Then I should not see any details for tagName "<tagName>"
    Examples:

      | tagName |
      | xx      |





