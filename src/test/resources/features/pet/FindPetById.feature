Feature: The pet service returns pets by id or updates an existing pet in the store or adds a new pet to the store


  @addPet-valid
  Scenario Outline: Add a pet into the store
    When I create a new pet whose details are id "<petId>",name "<petName>",categoryId "<categoryId>",categoryName "<categoryName>",photoUrls "<photoUrls>",tagsId "<tagId>",tagName "<tagName>",status "<status>"
    Then pet must be created whose details are id "<petId>",name "<petName>",categoryId "<categoryId>",categoryName "<categoryName>",photoUrls "<photoUrls>",tagsId "<tagId>",tagName "<tagName>",status "<status>"
    Examples:
      | petId | petName | categoryId | categoryName | photoUrls | tagId | tagName | status    |
      | 1     | cat1    | 1          | Cats         | url       | 1     | Cats    | available |

  @updatePet-valid
  Scenario Outline: update pet in the store
    When I update a pet whose id is "<petId>" with name "<petName>" and status "<petStatus>"
    Then pet must be updated whose id is "<petId>" with name "<petName>" and status "<petStatus>"

    Examples:
      | petId | petName | petStatus |
      | 1     | cat     | available |
      | 4     | Dog 1   | sold      |

  @updatePet-invalid
  Scenario Outline: Update pet in the store with an invalidId
    When I update a pet with an invalid id "<invalidPetId>",name "<petName>" and status "<petStatus>"
    Then I should see an error "<errorMessage>",statusCode "<responseStatusCode>" in the pet response

    Examples:
      | invalidPetId | errorMessage | responseStatusCode | petName | petStatus |
      | @!           | Input error  | 400                | cats    | sold      |


  @DeletePet-valid
  Scenario Outline: Delete a pet by petId
    When I delete a pet whose id is "<petId>"
    Then pet with "<petId>" must be deleted

    Examples:
      | petId |
      | 1     |

  @DeletePet-Invalid
  Scenario Outline: Delete a pet by petId-invalid
    When I delete a pet whose id "<petId>" is invalid
    Then I should see an error "<errorMessage>",statusCode "<responseStatusCode>" in the pet response

    Examples:
      | petId | errorMessage | responseStatusCode |
      | @     | Input error  | 400                |


  @getPetById
  Scenario Outline: Get pet by id

    When I retrieve pet by id "<petId>"
    Then I should see details of pet of id "<petId>"

    Examples:

      | petId |
      | 2    |


  @getPetById-invalid
  Scenario Outline: Get pet by Id-invalid

    When I retrieve pet by an invalid id "<invalidPetId>"
    Then I should see an error "<errorMessage>",statusCode "<responseStatusCode>" in the pet response

    Examples:

      | invalidPetId | errorMessage  | responseStatusCode |
      | 5000          | Pet not found | 404                |
      | @            | Input error   | 400                |