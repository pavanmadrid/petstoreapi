Feature: The pet findByStatus service returns pets by status


  @getPetInventory
  Scenario Outline: Get pet by status

    When I retrieve pet by status "<status>"
    Then I should see details of pet of status "<status>"

    Examples:

      | status    |
      | available |
      | pending   |
      | sold      |



  @getPetInventory-invalid
  Scenario Outline: Get pet by status-invalid

    When I retrieve pet by an invalid status "<status>"
    Then I should see an error "<errorMessage>",statusCode "<responseStatusCode>" in the response

    Examples:

      | status | errorMessage                 | responseStatusCode |
      | xx     | Input error: query parameter | 400                |





