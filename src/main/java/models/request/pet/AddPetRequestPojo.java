package models.request.pet;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import models.response.pet.petResponse.CategoryObject;
import models.response.pet.petResponse.TagsObject;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AddPetRequestPojo {

    private long id;
    private String name;
    private CategoryObject category;
    private List<String> photoUrls;
    private List<TagsObject> tags;
    private String status;

}
