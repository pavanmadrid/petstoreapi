package models.response.pet.petResponse;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)

public class FindByStatusResponsePojo {

    private long id;
    private CategoryObject category;
    private String name;
    private List<String> photoUrls;
    private List<TagsObject> tags;
    private String status;
}
