package stepDefinitions.services.user;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.HashMap;

public class LoginService extends BaseService {

    Response response = null;
    private static Logger logger = LoggerFactory.getLogger(LoginService.class);


    @Given("a valid username {string}")
    public void validateUsername(String userName){
        Assert.assertTrue(!userName.isEmpty());
    }


    @And("a valid password {string}")
    public void aValidPassword(String password) {
        Assert.assertTrue(!password.isEmpty());
    }


    @When("I log in to the petstore application with user {string} and password {string}")
    public void userLogin(String userName,String password) {
        try {
            String endpoint = properties.getProperty("userLoginEndpoint");
            HashMap<String, String> queryParams = new HashMap<>();
            queryParams.put("username", userName);
            queryParams.put("password", password);

            response = restDriver.getRequestWithQueryParams(endpoint,queryParams);
            logger.info("user login response:" + response.getBody().asPrettyString());
        } catch (Exception e) {
            logger.error("Exception while logging in to petstore:" + e.getMessage());
        }

    }

    @Then("login should be successful")
    public void loginShouldBeSuccessful() {
        TestUtils.validateResponseStatusCode(response,200);
        Assert.assertTrue(response.body().asString().toUpperCase().contains("LOGGED IN USER SESSION"));

    }


}
