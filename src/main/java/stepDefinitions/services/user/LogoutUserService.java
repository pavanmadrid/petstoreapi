package stepDefinitions.services.user;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

public class LogoutUserService extends BaseService {

    Response response = null;
    private static Logger logger = LoggerFactory.getLogger(LogoutUserService.class);

    @Given("a userName {string} is logged in")
    public void aUserNameIsLoggedIn(String userName) {
        Assert.assertTrue(!userName.isEmpty());
    }

    @When("userName {string} logs out")
    public void usernameLogsOut(String userName) {
        String endPoint = properties.getProperty("logoutApiEndpoint");
        response = restDriver.getRequest(endPoint);
        logger.info("logout response:" + response.getBody().asPrettyString());
    }


    @Then("userName {string} should be logged out")
    public void usernameShouldBeLoggedOut(String userName) {
        TestUtils.validateResponseStatusCode(response,200);
        TestUtils.validateResponseBodyContent(response,"User logged out");

    }


}
