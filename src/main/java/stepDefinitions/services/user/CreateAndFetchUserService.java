package stepDefinitions.services.user;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.request.user.UserRequestPojo;
import models.response.user.UserResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.HashMap;

public class CreateAndFetchUserService extends BaseService {

    Response response = null;
    Response fetchUserDetailsResponse = null;
    Response updateUserResponse = null;
    Response deleteUserResponse = null;
    private static Logger logger = LoggerFactory.getLogger(LoginService.class);
    UserRequestPojo userRequestPojo;
    UserResponsePojo userResponsePojo;


    @When("I create a new user of userName {string} ,id {string},firstName {string},lastName {string},email {string},password {string},phone {string},userStatus {string}")
    public void createNewUser(String userName, String id, String firstName, String lastName, String email, String password, String phone,String userStatus) {
            String endPoint = properties.getProperty("userEndpoint");
            userRequestPojo = new UserRequestPojo(Integer.parseInt(id),userName,firstName,lastName,email,password,phone,Integer.parseInt(userStatus));
            response = restDriver.postRequest(endPoint,userRequestPojo);
            logger.info("Create User response:" + response.body().asPrettyString());

    }


    @Then("a new user of userName {string} is successfully created")
    public void validateUserCreation(String userName) {
        TestUtils.validateResponseStatusCode(response,200);
        userResponsePojo = response.getBody().as(UserResponsePojo.class);
        Assert.assertEquals(userName,userResponsePojo.getUsername());
        Assert.assertEquals(userRequestPojo.getEmail(),userResponsePojo.getEmail());
        logger.info("Successfully created user:" + userName);

    }


    @When("I fetch the details for user {string}")
    public void iFetchTheDetailsForUser(String userName) {
        String endPoint = properties.getProperty("userEndpoint") + "/{username}";
        HashMap<String,String> pathParams = new HashMap<>();
        pathParams.put("username",userName);
        fetchUserDetailsResponse = restDriver.getRequestWithPathParams(endPoint,pathParams);
        logger.info("User details response for user:" + userName + ":" + fetchUserDetailsResponse.getBody().asPrettyString());
    }

    @Then("I should see the details of user {string}")
    public void iShouldSeeTheDetailsOfUser(String userName) {
        userResponsePojo = fetchUserDetailsResponse.getBody().as(UserResponsePojo.class);


    }

    @When("I fetch the details for an invalid user {string}")
    public void iFetchTheDetailsForAnInvalidUser(String invalidUsername) {
        iFetchTheDetailsForUser(invalidUsername);
    }

    @Then("I should see an error in the response for user {string}")
    public void validateErrorMessageForInvalidUser(String invalidUserName) {
        TestUtils.validateResponseBodyContent(fetchUserDetailsResponse,"User not found");
    }

    @When("I update the details for user {string}")
    public void iUpdateTheDetailsForUser(String userName) {

        //first fetch existing details for ths user
        iFetchTheDetailsForUser(userName);
        userResponsePojo = fetchUserDetailsResponse.getBody().as(UserResponsePojo.class);


        //now update the details for the user
        String endPoint = properties.getProperty("userEndpoint") + "/{username}";
        HashMap<String,String> pathParams = new HashMap<>();
        pathParams.put("username",userName);
        userRequestPojo = new UserRequestPojo(userResponsePojo.getId()+1, userName,userResponsePojo.getFirstName(),userResponsePojo.getLastName(),userResponsePojo.getEmail(),userResponsePojo.getPassword(),userResponsePojo.getPhone(),userResponsePojo.getUserStatus());
        updateUserResponse = restDriver.putRequestWithPathParams(endPoint,userRequestPojo,pathParams);

    }

    @Then("I should see the updated details of user {string}")
    public void iShouldSeeTheUpdatedDetailsOfUser(String userName) {
        userResponsePojo = updateUserResponse.getBody().as(UserResponsePojo.class);
        TestUtils.validateResponseStatusCode(updateUserResponse,200);
        Assert.assertEquals(userName,userResponsePojo.getUsername());
        Assert.assertEquals(userRequestPojo.getId(),userResponsePojo.getId());
    }



    @When("I delete the user {string}")
    public void iDeleteTheUser(String userName) {
        String endPoint = properties.getProperty("userEndpoint") + "/{username}";
        HashMap<String,String> pathParams = new HashMap<>();
        pathParams.put("username",userName);
        deleteUserResponse = restDriver.deleteRequestWithPathParams(endPoint,pathParams);
        logger.info("Delete User response:" + deleteUserResponse.body().asPrettyString());
    }

    @Then("user {string} should be deleted")
    public void userShouldBeDeleted(String userName) {
        TestUtils.validateResponseStatusCode(deleteUserResponse,200);
    }



}
