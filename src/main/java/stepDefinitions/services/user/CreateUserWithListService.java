package stepDefinitions.services.user;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;


import models.request.user.UserRequestPojo;
import models.response.user.UserResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.List;

public class CreateUserWithListService extends BaseService {

    Response response = null;
    private static Logger logger = LoggerFactory.getLogger(CreateUserWithListService.class);
    List<UserRequestPojo> userRequestPojo = new ArrayList<>();
    UserResponsePojo[] userResponsePojo;

    int numberOfUserNames = 0;



    @When("I create multiple users of userNames {string}")
    public void iCreateMultipleUsersOfUserNames(String userNames) {
        String endpoint = properties.getProperty("userMultipleEndpoint");
        String[] userNameSplit = userNames.split(";");
        numberOfUserNames = userNameSplit.length;
        for(int i=0;i<numberOfUserNames;i++){
            String userName = userNameSplit[i];
            userRequestPojo.add(i,new UserRequestPojo(1,userName,userName,userName,userName,userName,"100",1));
        }

        response=restDriver.postRequest(endpoint,userRequestPojo);
        logger.info("Multiple Users Response:" + response.getBody().asPrettyString());

    }

    @Then("multiple users of userNames {string} should be created")
    public void multipleUsersOfUserNamesShouldBeCreated(String userNames) {
        TestUtils.validateResponseStatusCode(response,200);
        for(int i=0;i < numberOfUserNames;i++){
            String expectedUserName = userRequestPojo.get(i).getUsername();
            userResponsePojo = response.getBody().as(UserResponsePojo[].class);
            String actualUserName = userResponsePojo[i].getUsername();
            Assert.assertEquals(expectedUserName,actualUserName);
        }
    }



}
