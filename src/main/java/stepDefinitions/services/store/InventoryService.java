package stepDefinitions.services.store;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.response.store.InventoryResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import stepDefinitions.services.user.LoginService;
import utils.TestUtils;

public class InventoryService extends BaseService {

    Response storeInventoryResponse = null;
    private InventoryResponsePojo inventoryResponsePojo = null;
    private static Logger logger = LoggerFactory.getLogger(InventoryService.class);

    @When("I want to retrieve pet inventory status")
    public void iWantToRetrievePetInventoryStatus() {
        String endPoint = properties.getProperty("storeInventoryEndpoint");
        storeInventoryResponse = restDriver.getRequest(endPoint);
        logger.info("Store Inventory Response:" + storeInventoryResponse.getBody().asPrettyString());
    }

    @Then("I should see details of store's pet inventory")
    public void getPetInventoriesStatus() {
        TestUtils.validateResponseStatusCode(storeInventoryResponse, 200);
        inventoryResponsePojo = storeInventoryResponse.getBody().as(InventoryResponsePojo.class);
        Assert.assertTrue(inventoryResponsePojo.getApproved() >= 0);

    }
}
