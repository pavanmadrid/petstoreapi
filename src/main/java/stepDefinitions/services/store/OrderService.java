package stepDefinitions.services.store;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import junit.framework.Test;
import models.request.store.OrderRequestPojo;
import models.response.store.FetchOrderResponsePojo;
import models.response.store.InventoryResponsePojo;
import models.response.store.StoreOrderResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.HashMap;

public class OrderService extends BaseService {

    Response response = null;
    private OrderRequestPojo orderRequestPojo = null;
    private StoreOrderResponsePojo storeOrderResponsePojo = null;
    private FetchOrderResponsePojo fetchOrderResponsePojo = null;
    private static Logger logger = LoggerFactory.getLogger(OrderService.class);

    @When("I place a pet order with details {string},{string},{string},{string},{string},{string}")
    public void iPlaceAPetOrderWithDetails(String id, String petId, String quantity, String shipDate, String status, String complete) {
        try {
            String endPoint = properties.getProperty("storeOrderEndpoint");
            orderRequestPojo = new OrderRequestPojo(Integer.parseInt(id),Integer.parseInt(petId),Integer.parseInt(quantity),shipDate,status,Boolean.parseBoolean(complete));
            response= restDriver.postRequest(endPoint,orderRequestPojo);
            logger.info("Store Order Response:" + response.getBody().asPrettyString());
        } catch (Exception e) {
            logger.error("Exception while placing a pet order:" + e.getMessage());
        }
    }

    @Then("the order with details {string},{string},{string},{string},{string},{string} must be successfully placed")
    public void theOrderWithDetailsMustBeSuccessfullyPlaced(String id, String petId, String quantity, String shipDate, String status, String complete) {
        try {
            storeOrderResponsePojo = response.getBody().as(StoreOrderResponsePojo.class);
            Assert.assertEquals(orderRequestPojo.getId(),storeOrderResponsePojo.getId());
            logger.info("");
        } catch (Exception e) {
            logger.error("Exception while validating pet order details:" + e.getMessage());
        }
    }

    @Given("a valid orderId {string}")
    public void aValidOrderId(String orderId) {
        int orderIdInt = Integer.parseInt(orderId);
        Assert.assertTrue(orderIdInt<=5 || orderIdInt >10);
    }

    @When("I retrieve the details for order {string}")
    public void iRetrieveTheDetailsForOrder(String orderId) {
        String endPoint = properties.getProperty("storeOrderEndpoint") + "/{orderId}";
        HashMap<String,String> pathParams = new HashMap<>();
        pathParams.put("orderId",orderId);
        response = restDriver.getRequestWithPathParams(endPoint,pathParams);
        logger.info("Fetch Order Response:" + response.getBody().asPrettyString());

    }

    @Then("I should see the details for order {string}")
    public void iShouldSeeTheDetailsForOrder(String orderId) {
        fetchOrderResponsePojo = response.getBody().as(FetchOrderResponsePojo.class);
        Assert.assertEquals(Integer.parseInt(orderId),fetchOrderResponsePojo.getId());
    }

    @Given("an invalid orderId {string}")
    public void anInvalidOrderId(String invalidOrderId) {
        Assert.assertTrue((Integer.parseInt(invalidOrderId) > 5 && Integer.parseInt(invalidOrderId) <10) || invalidOrderId.contains("@"));
    }

    @Then("I should see an error {string},errorCode {string} in the response for order {string}")
    public void iShouldSeeAnErrorInTheResponseForOrder(String errorMessage,String errorCode,String invalidOrderId) {
        TestUtils.validateResponseStatusCode(response,Integer.parseInt(errorCode));
        TestUtils.validateResponseBodyContent(response,errorMessage);
        logger.info("Error message found for invalid order:" + invalidOrderId);
    }

    @When("I delete the details for order {string}")
    public void iDeleteTheDetailsForOrder(String orderId) {
        String endPoint = properties.getProperty("storeOrderEndpoint") + "/{orderId}";
        HashMap<String,String> pathParams = new HashMap<>();
        pathParams.put("orderId",orderId);
        response = restDriver.deleteRequestWithPathParams(endPoint,pathParams);
    }

    @Then("order {string} should be deleted")
    public void orderShouldBeDeleted(String orderId) {
        TestUtils.validateResponseStatusCode(response,200);
        logger.info("Deleted the order:" + orderId);
    }
}
