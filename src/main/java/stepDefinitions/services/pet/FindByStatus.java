package stepDefinitions.services.pet;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.response.pet.petResponse.FindByStatusResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.HashMap;

public class FindByStatus extends BaseService {

    Response response = null;
    FindByStatusResponsePojo findByStatusResponsePojo[] = null;
    private static Logger logger = LoggerFactory.getLogger(FindByStatus.class);

    @When("I retrieve pet by status {string}")
    public void iRetrievePetByStatus(String status) {

        HashMap<String, String> queryParams = new HashMap<>();
        queryParams.put("status", status);
        String endPoint = properties.getProperty("petFindByStatusEndpoint");
        response = restDriver.getRequestWithQueryParams(endPoint, queryParams);

    }


    @Then("I should see details of pet of status {string}")
    public void iShouldSeeDetailsOfPetOfStatus(String status) {
        TestUtils.validateResponseStatusCode(response, 200);
        findByStatusResponsePojo = response.getBody().as(FindByStatusResponsePojo[].class);
        for (int i = 0; i < findByStatusResponsePojo.length; i++) {
            String actualStatus = findByStatusResponsePojo[i].getStatus();
            Assert.assertEquals(status.trim().toUpperCase(), actualStatus.toUpperCase().trim());
        }

    }

    @When("I retrieve pet by an invalid status {string}")
    public void iRetrievePetByAnInvalidStatus(String invalidStatus) {
        Assert.assertTrue(!invalidStatus.toUpperCase().equals("available") && !invalidStatus.toUpperCase().equals("pending") && !invalidStatus.toUpperCase().equals("sold"));
        iRetrievePetByStatus(invalidStatus);
    }

    @Then("I should see an error {string},statusCode {string} in the response")
    public void iShouldSeeAnErrorStatusCodeInTheResponse(String errorMessage, String statusCode) {
        TestUtils.validateResponseStatusCode(response,Integer.parseInt(statusCode));
        TestUtils.validateResponseBodyContent(response,errorMessage);
    }


}
