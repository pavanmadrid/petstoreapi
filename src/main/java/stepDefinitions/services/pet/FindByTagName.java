package stepDefinitions.services.pet;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.response.empty.EmptyResponsePojo;
import models.response.pet.petResponse.FindByStatusResponsePojo;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;

import java.util.HashMap;

public class FindByTagName extends BaseService {

    Response response = null;
    FindByStatusResponsePojo findByStatusResponsePojo[] = null;
    EmptyResponsePojo emptyResponsePojo[] = null;
    private static final Logger logger = LoggerFactory.getLogger(FindByTagName.class);

    @When("I retrieve pet by tagName {string}")
    public void iRetrievePetByTagName(String tagName) {
        String endPoint = properties.getProperty("petFindByTagNameEndPoint");
        HashMap<String,String> queryParams = new HashMap<>();
        queryParams.put("tags",tagName);
        response = restDriver.getRequestWithQueryParams(endPoint,queryParams);
    }

    @Then("I should see details of pet of tagName {string}")
    public void iShouldSeeDetailsOfPetOfTagName(String tagName) {

        findByStatusResponsePojo = response.getBody().as(FindByStatusResponsePojo[].class);
        int numberOfResultsReturned = findByStatusResponsePojo.length;
        for(int i=0;i<numberOfResultsReturned;i++){
            int tagNamesSize = findByStatusResponsePojo[i].getTags().size();
            for(int j=0;j<tagNamesSize;j++){
                String actualTagName = findByStatusResponsePojo[i].getTags().get(j).getName();
                Assert.assertEquals(tagName,actualTagName);
            }
        }
    }

    @When("I retrieve pet by an invalid tagName {string}")
    public void iRetrievePetByAnInvalidTagName(String invalidTagName) {
        Assert.assertTrue(!invalidTagName.equals("tag1") && !invalidTagName.equals("tag2"));
        iRetrievePetByTagName(invalidTagName);

    }


    @Then("I should not see any details for tagName {string}")
    public void iShouldNotSeeAnyDetailsForTagName(String invalidTagName) {
        emptyResponsePojo = response.getBody().as(EmptyResponsePojo[].class);
        Assert.assertTrue(emptyResponsePojo.length==0);
        logger.info("No details seen for invalid tagName:" + invalidTagName);

    }


}
