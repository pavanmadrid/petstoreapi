package stepDefinitions.services.pet;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import models.request.pet.AddPetRequestPojo;
import models.response.pet.petResponse.CategoryObject;
import models.response.pet.petResponse.FindByStatusResponsePojo;
import models.response.pet.petResponse.TagsObject;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import stepDefinitions.services.base.BaseService;
import utils.TestUtils;

import java.util.ArrayList;
import java.util.HashMap;

public class FindById extends BaseService {


    Response response = null;
    FindByStatusResponsePojo findByStatusResponsePojo;
    AddPetRequestPojo addPetRequestPojo;

    private static Logger logger = LoggerFactory.getLogger(FindById.class);

    @When("I retrieve pet by id {string}")
    public void iRetrievePetById(String petId) {
        String endPoint = properties.getProperty("petEndpoint") + "/{petId}";
        HashMap<String, String> pathParams = new HashMap<>();
        pathParams.put("petId", petId);
        response = restDriver.getRequestWithPathParams(endPoint, pathParams);
        logger.info("Pet Response:" + response.getBody().asPrettyString());

    }

    @Then("I should see details of pet of id {string}")
    public void iShouldSeeDetailsOfPetOfId(String petId) {
        findByStatusResponsePojo = response.getBody().as(FindByStatusResponsePojo.class);
        long id = findByStatusResponsePojo.getId();
        Assert.assertEquals(Integer.parseInt(petId), id);


    }

    @When("I retrieve pet by an invalid id {string}")
    public void iRetrievePetByAnInvalidId(String invalidPetId) {
           iRetrievePetById(invalidPetId);
    }

    @Then("I should see an error {string},statusCode {string} in the pet response")
    public void iShouldSeeAnErrorStatusCodeInTheResponse(String errorMessage, String statusCode) {
        TestUtils.validateResponseStatusCode(response,Integer.parseInt(statusCode));
        TestUtils.validateResponseBodyContent(response,errorMessage);
    }

    @When("I update a pet whose id is {string} with name {string} and status {string}")
    public void iUpdateAPetWhoseIdIsWithNameAndStatus(String petId, String petName, String petStatus) {
        String endPoint = properties.getProperty("petEndpoint") + "/{petId}";
        HashMap pathParams = new HashMap();
        pathParams.put("petId",petId);
        HashMap queryParams = new HashMap();
        queryParams.put("name",petName);
        queryParams.put("status",petStatus);
        response = restDriver.postRequestWithPathAndQueryParams(endPoint,"",pathParams,queryParams);
    }

    @Then("pet must be updated whose id is {string} with name {string} and status {string}")
    public void petMustBeUpdatedWhoseIdIsWithNameAndStatus(String petId, String petName, String petStatus) {
        TestUtils.validateResponseStatusCode(response,200);
        findByStatusResponsePojo = response.getBody().as(FindByStatusResponsePojo.class);
        Assert.assertEquals(Integer.parseInt(petId),findByStatusResponsePojo.getId());
        Assert.assertEquals(petName,findByStatusResponsePojo.getName());
        Assert.assertEquals(petStatus,findByStatusResponsePojo.getStatus());
    }


    @When("I update a pet with an invalid id {string},name {string} and status {string}")
    public void iUpdateAPetWithAnInvalidIdNameAndStatus(String invalidPetId, String petName, String petStatus) {
        iUpdateAPetWhoseIdIsWithNameAndStatus(invalidPetId,petName,petStatus);
    }

    @When("I delete a pet whose id is {string}")
    public void iDeleteAPetWhoseIdIs(String petId) {
     String endPoint = properties.getProperty("petEndpoint") + "/{petId}";
        HashMap pathParams = new HashMap();
        pathParams.put("petId",petId);
        response = restDriver.deleteRequestWithPathParams(endPoint,pathParams);

    }

    @Then("pet with {string} must be deleted")
    public void petWithMustBeDeleted(String petId) {
        TestUtils.validateResponseStatusCode(response,200);
        TestUtils.validateResponseBodyContent(response,"Pet deleted");
    }

    @When("I delete a pet whose id {string} is invalid")
    public void iDeleteAPetWhoseIdIsInvalid(String invalidPetId) {
        iDeleteAPetWhoseIdIs(invalidPetId);
    }

    @When("I create a new pet whose details are id {string},name {string},categoryId {string},categoryName {string},photoUrls {string},tagsId {string},tagName {string},status {string}")
    public void iCreateANewPetWhoseDetailsAreIdNameCategoryIdCategoryNamePhotoUrlsTagsIdTagNameStatus(String petId, String petName, String categoryId, String categoryName, String photoUrls, String tagId, String tagName, String status) {
        String endPoint = properties.getProperty("petEndpoint") ;

        CategoryObject categoryObject = new CategoryObject(Integer.parseInt(categoryId),categoryName);
        ArrayList<String> photoUrlsList = new ArrayList<>();
        photoUrlsList.add(photoUrls);
        TagsObject tagsObject = new TagsObject(Integer.parseInt(tagId),tagName);
        ArrayList<TagsObject> tagsObjectArrayList = new ArrayList<>();
        tagsObjectArrayList.add(tagsObject);
        addPetRequestPojo = new AddPetRequestPojo(Integer.parseInt(petId),petName,categoryObject,photoUrlsList,tagsObjectArrayList,status);

        response = restDriver.postRequest(endPoint,addPetRequestPojo);
        logger.info("Create New Pet Response:" + response.getBody().asPrettyString());
    }

    @Then("pet must be created whose details are id {string},name {string},categoryId {string},categoryName {string},photoUrls {string},tagsId {string},tagName {string},status {string}")
    public void petMustBeCreatedWhoseDetailsAreIdNameCategoryIdCategoryNamePhotoUrlsTagsIdTagNameStatus(String petId, String petName, String categoryId, String arg3, String arg4, String arg5, String arg6, String arg7) {
        findByStatusResponsePojo = response.getBody().as(FindByStatusResponsePojo.class);
        Assert.assertEquals(Integer.parseInt(petId),findByStatusResponsePojo.getId());
    }
}
